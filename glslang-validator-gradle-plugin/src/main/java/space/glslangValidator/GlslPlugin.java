package space.glslangValidator;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.internal.plugins.DslObject;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.TaskProvider;

import javax.inject.Inject;
import java.util.Map;

public class GlslPlugin implements Plugin<Project> {
	
	@Inject
	public GlslPlugin() {
	}
	
	@Override
	public void apply(Project project) {
		project.getPlugins().withType(JavaBasePlugin.class, appliedPlugin -> {
			
			//glsl config extension
			project.getExtensions().create(GlslConfigurationExtension.EXTENSION_NAME, GlslConfigurationExtension.class, project);
			
			//each sourceSets
			JavaPluginConvention javaPluginConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
			javaPluginConvention.getSourceSets().all(sourceSet -> {
				
				//glsl SourceSet
				final GlslSourcesSet glslSourcesSet = new GlslSourcesSet(sourceSet, project);
				new DslObject(sourceSet).getConvention().getPlugins().put("glsl", glslSourcesSet);
				
				//compile task
				TaskProvider<GlslCompileTask> compileTask = project.getTasks().register(sourceSet.getCompileTaskName("glsl"), GlslCompileTask.class, task -> {
					task.source(glslSourcesSet.getGlsl());
					task.getDestinationDir().set(glslSourcesSet.getGlsl().getDestinationDirectory());
				});
				sourceSet.getOutput().dir(Map.of("builtBy", compileTask.getName()), glslSourcesSet.getGlsl().getOutputDir());
			});
		});
	}
}
