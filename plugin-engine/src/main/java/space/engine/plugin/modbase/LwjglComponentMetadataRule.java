package space.engine.plugin.modbase;

import org.gradle.api.artifacts.CacheableRule;
import org.gradle.api.artifacts.ComponentMetadataContext;
import org.gradle.api.artifacts.ComponentMetadataRule;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Automatically adds lwjgl native jars to all lwjgl dependencies.
 * <p>
 * <ul>
 * <li>{@link #nativeVariants} contains all variant names of the jars that should be used, usually the platform names</li>
 * <li>{@link #nativeOverrides} maps an artifact name to a variant filter, use for artifacts which do not need all native variants.</li>
 * </ul>
 */
@CacheableRule
public class LwjglComponentMetadataRule implements ComponentMetadataRule {
	
	//static
	public static final HashSet<String> nativeVariants = new HashSet<>(List.of(
			"natives-linux",
			"natives-windows",
			"natives-macos"
	));
	
	public static final HashMap<String, Set<String>> nativeOverrides = new HashMap<>();
	
	public static void addNativeOverrides(String name, String... override) {
		addNativeOverrides(name, Set.of(override));
	}
	
	public static void addNativeOverrides(String name, Set<String> override) {
		nativeOverrides.put(name, override);
	}
	
	static {
		addNativeOverrides("lwjgl-vulkan", "natives-macos");
		addNativeOverrides("lwjgl-bom", Set.of());
	}
	
	//object
	@SuppressWarnings("CodeBlock2Expr")
	public void execute(ComponentMetadataContext context) {
		if (context.getDetails().getId().getGroup().equals("org.lwjgl")) {
			context.getDetails().allVariants(meta -> {
				meta.withFiles(files -> {
					@Nullable Set<String> override = nativeOverrides.get(context.getDetails().getId().getModule().getName());
					for (String nativeNariant : (override == null ? nativeVariants : override.stream().filter(nativeVariants::contains).collect(Collectors.toList()))) {
						files.addFile(context.getDetails().getId().getName() + "-" + context.getDetails().getId().getVersion() + "-" + nativeNariant + ".jar");
					}
				});
			});
		}
	}
}
