package space.engine.plugin.modbase;

import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaLibraryPlugin;
import org.gradle.api.plugins.JavaPluginConvention;

@NonNullApi
public class SpaceBasePlugin implements Plugin<Project> {
	
	public static final String TASK_PREPARE = "prepare";
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply(JavaLibraryPlugin.class);
		
		project.getTasks().register(TASK_PREPARE, t -> {
			JavaPluginConvention javaConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
			javaConvention.getSourceSets().all(s -> t.dependsOn(s.getAllSource()));
		});
		project.getDependencies().getComponents().all(LwjglComponentMetadataRule.class);
	}
}
