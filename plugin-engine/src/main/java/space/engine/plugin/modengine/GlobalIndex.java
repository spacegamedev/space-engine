package space.engine.plugin.modengine;

import org.gradle.api.Project;
import org.gradle.api.file.Directory;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskProvider;
import space.engine.plugin.modbase.SpaceBasePlugin;

public class GlobalIndex {
	
	public static final String PACKAGE_PATH = "space.engine.pack";
	public static final String CLASS_NAME = "GlobalIndex";
	
	public static void registerLoaderClassGenerateTask(Project project) {
		Provider<Directory> outputDirLoader = project.getLayout().getBuildDirectory().dir("jandexPack/loader");
		SourceSet sourceSet = project.getConvention().getPlugin(JavaPluginConvention.class).getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);
		TaskProvider<JandexLoaderClassTask> taskGenerateLoader = project.getTasks().register("generateGlobalJandexLoaderClass", JandexLoaderClassTask.class, t -> {
			t.getOutputDirectory().value(outputDirLoader);
			t.getPackagePath().value(GlobalIndex.PACKAGE_PATH);
			t.getClassName().value(GlobalIndex.CLASS_NAME);
		});
		sourceSet.getJava().srcDir(outputDirLoader);
		project.getTasks().named(sourceSet.getCompileJavaTaskName()).configure(t -> t.dependsOn(taskGenerateLoader));
		project.getTasks().named(SpaceBasePlugin.TASK_PREPARE).configure(t -> t.dependsOn(taskGenerateLoader));
	}
	
}
