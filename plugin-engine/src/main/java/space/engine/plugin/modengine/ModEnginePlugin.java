package space.engine.plugin.modengine;

import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.Directory;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskProvider;
import space.engine.plugin.modbase.SpaceBasePlugin;

@NonNullApi
public class ModEnginePlugin implements Plugin<Project> {
	
	public static final String TASK_GROUP_MOD = "space-mod";
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply(SpaceBasePlugin.class);
		
		//jandex indexing
		JavaPluginConvention javaConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
		javaConvention.getSourceSets().all(sourceSet -> {
			//constants
			Provider<String> packagePath = project.provider(() -> ClassnameUtils.toValidJavaPackageIdentifier(project.getGroup() + ".jandex"));
			Provider<String> className = project.provider(() -> ClassnameUtils.toValidJavaClassIdentifier(project.getName() + "-Index"));
			
			//dependencies required
			if (!("space.engine" == project.getGroup() && "jandex".equals(project.getName())))
				project.getDependencies().add("implementation", "space.engine:jandex");
			
			//generate loader class
			Provider<Directory> outputDirLoader = project.getLayout().getBuildDirectory().dir("jandex/loader/" + sourceSet.getName());
			TaskProvider<JandexLoaderClassTask> taskGenerateLoader = project.getTasks().register(sourceSet.getTaskName("generate", "JandexLoaderClass"), JandexLoaderClassTask.class, t -> {
				t.setGroup(TASK_GROUP_MOD);
				t.setDescription("generates a Class for easy loading of the Projects Index");
				
				t.getOutputDirectory().value(outputDirLoader);
				t.getPackagePath().value(packagePath);
				t.getClassName().value(className);
			});
			sourceSet.getJava().srcDir(project.files(outputDirLoader).builtBy(taskGenerateLoader));
			
			//generate index
			Provider<Directory> outputDirIndex = project.getLayout().getBuildDirectory().dir("jandex/index/" + sourceSet.getName());
			TaskProvider<JandexIndexTask> taskGenerateIndex = project.getTasks().register(sourceSet.getTaskName("generate", "JandexIndex"), JandexIndexTask.class, t -> {
				t.setGroup(TASK_GROUP_MOD);
				t.setDescription("generates an Index from all classes of this project");
				
				t.getOutputDirectory().value(outputDirIndex);
				t.getPackagePath().value(packagePath);
				t.getClassName().value(className);
				
				t.source(sourceSet.getJava().getDestinationDirectory());
			});
			sourceSet.getOutput().dir(project.files(outputDirIndex).builtBy(taskGenerateIndex));
		});
	}
}
