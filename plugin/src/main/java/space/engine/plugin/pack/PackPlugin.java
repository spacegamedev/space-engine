package space.engine.plugin.pack;

import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.Directory;
import org.gradle.api.file.FileCollection;
import org.gradle.api.plugins.ApplicationPlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.Sync;
import org.gradle.api.tasks.TaskProvider;
import space.engine.plugin.modbase.SpaceBasePlugin;
import space.engine.plugin.modengine.GlobalIndex;

@NonNullApi
public class PackPlugin implements Plugin<Project> {
	
	public static final String TASK_GROUP_PACK = "space-pack";
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply(SpaceBasePlugin.class);
		project.getPlugins().apply(ApplicationPlugin.class);
		
		//task mergeGlobalJandex
		SourceSet sourceSet = project.getConvention().getPlugin(JavaPluginConvention.class).getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);
		Provider<Directory> outputDirIndex = project.getLayout().getBuildDirectory().dir("jandexPack/index");
		TaskProvider<JandexMergeTask> mergeJandexTask = project.getTasks().register("mergeGlobalJandex", JandexMergeTask.class, t -> {
			t.setGroup(TASK_GROUP_PACK);
			t.setDescription("Merges all indeces from the RuntimeClasspath");
			
			t.getOutputDirectory().value(outputDirIndex);
			t.getPackagePath().value(GlobalIndex.PACKAGE_PATH);
			t.getClassName().value(GlobalIndex.CLASS_NAME);
			
			//does NOT include this project's output
			Configuration runtimeClasspath = project.getConfigurations().getByName(JavaPlugin.RUNTIME_CLASSPATH_CONFIGURATION_NAME);
			t.source(project.provider(() -> runtimeClasspath.getFiles().stream().map(f -> f.isFile() ? project.zipTree(f) : f).toArray()));
			t.include("**/*.idx");
			t.dependsOn(runtimeClasspath);
		});
		sourceSet.getOutput().dir(project.files(outputDirIndex).builtBy(mergeJandexTask));
		
		//task extractRuntimeClasspath
		project.getTasks().register("extractRuntimeClasspath", Sync.class, t -> {
			t.setDescription("copies together the entire runtimeClasspath for debugging purposes");
			
			JavaPluginConvention javaConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
			//includes everything, also this project
			FileCollection runtimeClasspath = javaConvention.getSourceSets().getByName("main").getRuntimeClasspath();
			t.from(project.files(project.provider(() -> runtimeClasspath.getFiles().stream().map(f -> f.isFile() ? project.zipTree(f) : f).toArray())).builtBy(runtimeClasspath));
			t.into(project.getLayout().getBuildDirectory().dir("extractRuntimeClasspath"));
		});
	}
}
