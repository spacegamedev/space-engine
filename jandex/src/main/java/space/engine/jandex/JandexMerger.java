package space.engine.jandex;

import org.jboss.jandex.AnnotationInstance;
import org.jboss.jandex.ClassInfo;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Index;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JandexMerger {
	
	private static final Field INDEX_ANNOTATIONS;
	private static final Field INDEX_SUBCLASSES;
	private static final Field INDEX_IMPLEMENTORS;
	private static final Field INDEX_CLASSES;
	
	static {
		try {
			INDEX_ANNOTATIONS = findPrivateField("annotations");
			INDEX_SUBCLASSES = findPrivateField("subclasses");
			INDEX_IMPLEMENTORS = findPrivateField("implementors");
			INDEX_CLASSES = findPrivateField("classes");
		} catch (NoSuchFieldException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	private static @NotNull Field findPrivateField(@NotNull String fieldName) throws NoSuchFieldException {
		Field field = Index.class.getDeclaredField(fieldName);
		field.setAccessible(true);
		return field;
	}
	
	//methods
	@SuppressWarnings("unchecked")
	public static Index merge(Index... indexes) {
		try {
			Map<DotName, List<AnnotationInstance>> annotations = new HashMap<>();
			Map<DotName, List<ClassInfo>> subclasses = new HashMap<>();
			Map<DotName, List<ClassInfo>> implementors = new HashMap<>();
			Map<DotName, ClassInfo> classes = new HashMap<>();
			
			for (Index index : indexes) {
				((Map<DotName, List<AnnotationInstance>>) INDEX_ANNOTATIONS.get(index)).forEach((k, v) -> annotations.computeIfAbsent(k, l -> new ArrayList<>()).addAll(v));
				((Map<DotName, List<ClassInfo>>) INDEX_SUBCLASSES.get(index)).forEach((k, v) -> subclasses.computeIfAbsent(k, l -> new ArrayList<>()).addAll(v));
				((Map<DotName, List<ClassInfo>>) INDEX_IMPLEMENTORS.get(index)).forEach((k, v) -> implementors.computeIfAbsent(k, l -> new ArrayList<>()).addAll(v));
				classes.putAll((Map<DotName, ClassInfo>) INDEX_CLASSES.get(index));
			}
			
			return Index.create(annotations, subclasses, implementors, classes);
		} catch (IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
}
