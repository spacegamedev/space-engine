package space.engine.jandex;

import org.jboss.jandex.Index;
import org.jboss.jandex.IndexReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JandexUtils {
	
	public static final String IO_ERROR_MESSAGE = "File has to exist and be readable";
	
	public static Index readIndex(File file) throws IOException {
		try (InputStream in = new FileInputStream(file)) {
			return new IndexReader(in).read();
		}
	}
	
	public static Index readIndexAssertExists(File file) {
		try {
			return readIndex(file);
		} catch (IOException e) {
			throw new AssertionError(IO_ERROR_MESSAGE, e);
		}
	}
	
	public static Index readIndex(Class<?> folder, String name) throws IOException {
		try (InputStream in = folder.getResourceAsStream(name)) {
			return new IndexReader(in).read();
		}
	}
	
	public static Index readIndexAssertExists(Class<?> folder, String name) {
		try {
			return readIndex(folder, name);
		} catch (IOException e) {
			throw new AssertionError(IO_ERROR_MESSAGE, e);
		}
	}
}
